#include "Dot.hpp"

namespace entity
{
	Dot::Dot(const vas::Vector2 & position):
		sprite(std::make_unique<vas::Sprite>("assets/textures/dot.png", position))
	{
		/*movement.x = 2.0f * uniRealDistribution(engine);
		movement.y = 2.0f * uniRealDistribution(engine);*/
	}

	Dot::Dot(const vas::Vector2 & position, const vas::Vector2 & initialMovement):
		sprite(std::make_unique<vas::Sprite>("assets/textures/dot.png", position)), movement(initialMovement)
	{
	}

	Dot::~Dot()
	{
	}

	void Dot::tick()
	{
		if (sprite->getPosition().x >= (1920.0f - 64.0f))
			movement.x = -(2.0f * uniRealDistribution(engine));
		else if (sprite->getPosition().x <= 0)
			movement.x = 2.0f * uniRealDistribution(engine);

		if (sprite->getPosition().y >= (1080.0f - 64.0f))
			movement.y = -(2.0f * uniRealDistribution(engine));
		else if (sprite->getPosition().y <= 0)
			movement.y = 2.0f * uniRealDistribution(engine);

		sprite->move(movement);
		sprite->tick();
	}

	void Dot::draw()
	{
		sprite->draw();
	}
}