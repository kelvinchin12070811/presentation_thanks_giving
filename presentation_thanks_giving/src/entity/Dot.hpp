#pragma once
#include <random>
#include "../libraries/vasframework/graphics/IRenderAble.hpp"
#include "../libraries/vasframework/graphics/sprites/Sprite.hpp"

namespace entity
{
	class Dot : public vas::IRendererAble
	{
	public:
		Dot(const vas::Vector2& position);
		Dot(const vas::Vector2& position, const vas::Vector2& initialMovement);
		~Dot();

		void tick() override;
		void draw() override;
	private:
		std::uniform_real_distribution<float> uniRealDistribution{ 0.001f, 1.0f };
		std::random_device rd;
		std::mt19937 engine{ rd() };
		std::unique_ptr<vas::Sprite> sprite = nullptr;
		vas::Vector2 movement;
	};
}