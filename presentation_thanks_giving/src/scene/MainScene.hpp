#pragma once
#include <vector>
#include <array>
#include "../libraries/vasframework/graphics/scene/AbstractScene.hpp"
#include "../libraries/vasframework/graphics/sprites/Sprite.hpp"
#include "../entity/Dot.hpp"

using namespace vas;
using namespace sdl;
using namespace std;

namespace scene
{
	class MainScene : public AbstractScene
	{
	public:
		MainScene();
		~MainScene();

		void tick() override;
		void draw() override;

		void eventSlots(sdl::Event& ev);
	private:
		unique_ptr<Sprite> background{ nullptr };
		unique_ptr<Sprite> backgroundCover{ nullptr };
		array<unique_ptr<Sprite>, 5> boards;
		vector<unique_ptr<entity::Dot>> dots;

		int8_t boardIndex{ 0 };
	};
}