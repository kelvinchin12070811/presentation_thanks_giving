#include <Windows.h>
#include "libraries/vasframework/base/Base.hpp"
#include "libraries/vasframework/manager/SceneManager.hpp"
#include "libraries/vasframework/util/CommonTools.hpp"
#include "libraries/vasframework/util/TextTools.hpp"
#include "scene/MainScene.hpp"
#include "ConfigManager.hpp"

using namespace std;
using namespace vas;

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR cmdLine, int cmdShow)
{
	CommonTools::getInstance().setAssistanceName("宮本サクラ");
	try
	{
		ConfigManager::getInstance().openConfig("config.yaml");
		{
			if (ConfigManager::getInstance()["show_console"].as<bool>())
			{
				AllocConsole();
				freopen("CONIN$", "r+t", stdin);
				freopen("CONIN$", "w+t", stdout);
				CommonTools::getInstance().hasConsole = true;
			}
		}
		uint32_t flags = sdl::Window::Flags::fullscreen_desktop;
		if (ConfigManager::getInstance()["native_fullscreen"].as<bool>())
			flags = sdl::Window::Flags::fullscreen;

		Base::getInstance().init();
		sdl::Point canvasSize(ConfigManager::getInstance()["canvas_size"][0].as<int>(), ConfigManager::getInstance()["canvas_size"][1].as<int>());
		sdl::Point windowSize = canvasSize;
		if (!ConfigManager::getInstance()["fullscreen"].as<bool>())
		{
			flags = sdl::Window::Flags::shown;
			windowSize = sdl::Point(ConfigManager::getInstance()["window_size"][0].as<int>(), ConfigManager::getInstance()["window_size"][1].as<int>());
		}
		sdl::Window mainWindow("thanks giving presentation - VAS Framework demo", windowSize, flags);
		if (mainWindow == sdl::emptycomponent) throw sdl::SDLCoreException();

		sdl::Renderer mainRenderer(mainWindow, sdl::Renderer::defIndex, sdl::Renderer::RendererFlags::accelerated);
		if (mainRenderer == sdl::emptycomponent) throw sdl::SDLCoreException();

		mainRenderer.setLogicalSize(canvasSize);

		Base::getInstance().Window() = std::move(mainWindow);
		Base::getInstance().Renderer() = std::move(mainRenderer);

		SceneManager::getInstance().call(std::make_shared<scene::MainScene>());
		Base::getInstance().startGameLoop();
		Base::getInstance().cleanAndQuit();
	}
	catch (const exception& e)
	{
		Base::getInstance().cleanAndQuit();
		return CommonTools::getInstance().messenger(e.what(), CommonTools::MessageType::error, -1);
	}
	return 0;
}