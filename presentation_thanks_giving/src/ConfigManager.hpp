#pragma once
#include <string>
#include <yaml-cpp/yaml.h>

class ConfigManager
{
public:
	static ConfigManager& getInstance();

	void openConfig(const std::string& file);
	YAML::Node& getRoot();

	YAML::Node operator[](const std::string& name);
private:
	ConfigManager();
	~ConfigManager();
	YAML::Node root;
};

