#include "ConfigManager.hpp"

ConfigManager & ConfigManager::getInstance()
{
	static ConfigManager instance;
	return instance;
}

void ConfigManager::openConfig(const std::string & file)
{
	auto document = YAML::LoadFile(file);
	if (!document["config"])
		throw std::runtime_error("Cannot found key \"config\" from \"" + file + "\". It might not a proper config file");

	root = document["config"];
	if (root["magic"].as<std::string>() != "CC50")
		throw std::runtime_error("Magic number not found in \"" + file + "\". It might not a proper config file");
}

YAML::Node & ConfigManager::getRoot()
{
	return root;
}

YAML::Node ConfigManager::operator[](const std::string & name)
{
	return root[name];
}

ConfigManager::ConfigManager()
{
}

ConfigManager::~ConfigManager()
{
}