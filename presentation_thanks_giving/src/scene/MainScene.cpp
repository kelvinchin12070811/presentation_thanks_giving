#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include "MainScene.hpp"
#include "../libraries/vasframework/util/CommonTools.hpp"
#include "../libraries/vasframework/base/Base.hpp"
#include "../libraries/vasframework/manager/InputManager.hpp"
#include "../ConfigManager.hpp"

namespace scene
{
	MainScene::MainScene()
	{
		{
			auto& evSignal = Base::getInstance().EventProcessorSignal(Base::SignalsType::EventProcessor::preEventLoop);
			evSignal.connect(boost::bind(&scene::MainScene::eventSlots, this, boost::placeholders::_1));
		}
		CommonTools::getInstance().messenger("MainScene is called");

		background = make_unique<Sprite>("assets/textures/background.png", Vector2(0, 0));
		backgroundCover = make_unique<Sprite>("assets/textures/background_cover.png", Vector2());
		dots.resize(ConfigManager::getInstance()["dots_count"].as<size_t>());
		CommonTools::getInstance().messenger((boost::format("particle counts: %d") % dots.size()).str());

		//Generate dots
		{
			std::random_device rd;
			std::mt19937 engine;
			std::uniform_int_distribution<int> generator(0, 3);
			for (auto itr = dots.begin(); itr != dots.end(); itr++)
			{
				Vector2 position;
				uint8_t coner = static_cast<uint8_t>(generator(engine));
				switch (coner)
				{
				case 0:
					position = Vector2(-64, -64);
					break;
				case 1:
					position = Vector2(1984, -64);
					break;
				case 2:
					position = Vector2(-64, 1144);
					break;
				case 3:
					position = Vector2(1984, 1144);
				}
				*itr = make_unique<entity::Dot>(position);
			}

			//Load board
			boards[0] = make_unique<Sprite>("assets/textures/texts.png", Vector2());
			boards[1] = make_unique<Sprite>("assets/textures/lucky-draw.png", Vector2());
			boards[2] = make_unique<Sprite>("assets/textures/3rd-prize.png", Vector2());
			boards[3] = make_unique<Sprite>("assets/textures/2nd-prize.png", Vector2());
			boards[4] = make_unique<Sprite>("assets/textures/1st-prize.png", Vector2());
		}
	}

	MainScene::~MainScene()
	{
	}

	void MainScene::tick()
	{
		background->tick();
		for (auto itr = dots.begin(); itr != dots.end(); itr++)
		{
			(*itr)->tick();
		}
		backgroundCover->tick();
		boards[boardIndex]->tick();
	}

	void MainScene::draw()
	{
		background->draw();
		for (auto itr = dots.begin(); itr != dots.end(); itr++)
		{
			(*itr)->draw();
		}
		backgroundCover->draw();
		boards[boardIndex]->draw();
	}

	void MainScene::eventSlots(sdl::Event & ev)
	{
		switch (ev.type())
		{
		case EventType::keydown:
			if (InputManager::getInstance().isKeyTriggeredEv(Keycode::escape))
			{
				ev.pushEvent(EventType::quit);
			}
			else if (InputManager::getInstance().isKeyTriggeredEv(Keycode::right))
			{
				boardIndex++;
				if (boardIndex >= boards.size()) boardIndex = 0;
			}
			else if (InputManager::getInstance().isKeyTriggeredEv(Keycode::left))
			{
				boardIndex--;
				if (boardIndex < 0) boardIndex = boards.size() - 1;
			}
			break;
		}
	}
}